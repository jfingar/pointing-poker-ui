let env = document.location.host.includes("agile-story-pointing.com") ? "production" : "development";
let wsApiEndpoint = env === "development" ?
    'ws://' + PointingPokerConfig.env.local_websocket_domain + ':' + PointingPokerConfig.env.local_websocket_port :
    'wss://agile-story-pointing.com/wss/';

let state = {
    "clientId": null,
    "sessionId": null,
    "pointingFistOfFive": false,
    "sessionLabel": null,
    "participants": {},
    "revealScores": false,
    "activeTicket": null,
    "pointHistory": [],
    "thisParticipant": null,
    "sessionModerator": null
};

$(document).ready(function(){
    state.sessionId = window.sessionStorage.getItem("pointingPokerSessionId");
    state.thisParticipant = JSON.parse(window.sessionStorage.getItem("participant"));
    state.clientId = window.sessionStorage.getItem("pointingPokerClientId") ? parseInt(window.sessionStorage.getItem("pointingPokerClientId")) : null;
    
    // initialize ping - pong to prevent stale web socket connections based on browser limitations.
    setInterval(function(){
        sendMessage("pingPong", {"ping" : "PING"});
    }, 60000);
});

let conn = new WebSocket(wsApiEndpoint);
conn.onopen = () => {
    console.log("Connection established!");

    if (state.sessionId) {
        let userSessionRefreshPayload = {};
        if (state.thisParticipant) {
            userSessionRefreshPayload.participant = state.thisParticipant;
            userSessionRefreshPayload.participant.clientId = null;
        }
        userSessionRefreshPayload.sessionId = state.sessionId;
        userSessionRefreshPayload.oldClientId = state.clientId;
        sendMessage("refreshUserSession", userSessionRefreshPayload);
        activeSessionState();
    } else {
        sendMessage("getAvailableSessions", {});
        sendMessage("getClientId", {});
        selectSessionState();
    }
};

conn.onclose

conn.onmessage = e => {
    let response = JSON.parse(e.data);

    if (!response.actionLabel) {
        throw new Error("Missing required actionLabel from web sockets server.");
    }
    switch (response.actionLabel) {
        case "pingPong":
            // do nothing
            break;
        case "getAvailableSessions":
            drawSessionSelectTable(response.sessions);
            break;
        case "getClientId":
            updateClientId(response.clientId);
            break;
        case "refreshUserSession":
            if (response.oldClientId === state.clientId) {
                updateClientId(response.newClientId);
            }
            defaultMessageHandler(response);
            break;
        case "addParticipant":
            let participant = JSON.stringify({
                "name": $('#participant-name').val(),
                "clientId": state.clientId
            });
            window.sessionStorage.setItem("participant", participant);
            defaultMessageHandler(response);
            break;
        case "removeParticipant":
            window.sessionStorage.removeItem("participant");
            state.thisParticipant = null;
            defaultMessageHandler(response);
            break;
        case "clearSession":
        case "enterSession":
        case "exitSession":
        case "addTicket":
        case "closeTicket":
        case "hideScores":
        case "revealScores":
        case "updateParticipant":
        case "toggleFistOfFive":
        default:
            defaultMessageHandler(response);
    }
};

let updateClientId = clientId => {
    window.sessionStorage.setItem("pointingPokerClientId", clientId);
    state.clientId = clientId;

    // update browser session participant obj with new clientId
    let existingParticipant = window.sessionStorage.getItem("participant");
    if (existingParticipant) {
        existingParticipant = JSON.parse(existingParticipant);
        existingParticipant.clientId = clientId;
        window.sessionStorage.setItem("participant", JSON.stringify(existingParticipant));
    }
};

let defaultMessageHandler = response => {
    if (state.sessionId && response.pointingSession) {
        state.participants = response.pointingSession.participants;
        state.revealScores = response.pointingSession.revealScores;
        state.activeTicket = response.pointingSession.activeTicket;
        state.ticketHistory = response.pointingSession.ticketHistory;
        state.sessionLabel = response.pointingSession.sessionLabel;
        state.pointingFistOfFive = response.pointingSession.pointingFistOfFive;
    }
    setCurrentParticipant();
    setSessionModerator();
    refreshUiState();
};

let refreshUiState = () => {
    if (!state.sessionId) {
        selectSessionState();
    } else {
        activeSessionState();

        if (state.thisParticipant) {
            $('#participant-name').val(state.thisParticipant.name);
            $('#poker-session').show();
        }

        moderatorSettingsState();
        drawScoreOptions();

        if (state.pointingFistOfFive) {
            if (state.revealScores) {
                revealedTicketState();
            } else {
                pendingTicketState();
            }
        } else {
            if (state.activeTicket) {
                if (state.revealScores) {
                    revealedTicketState();
                } else {
                    pendingTicketState();
                }
            } else {
                noActiveTicketState();
            }
        }

        drawParticipantsTable();
        fistOfFiveState();
    }
}

let setCurrentParticipant = () => {
    let thisParticipantFound = false;
    for(let i in state.participants) {
        if (parseInt(state.participants[i].clientId) === parseInt(state.clientId)) {
            state.thisParticipant = state.participants[i];
            thisParticipantFound = true;
        }
    }
    if (!thisParticipantFound) {
        state.thisParticipant = null;
        // $('#participant-name').val("");
    } else {
        let participant = {
            "clientId": state.clientId,
            "name": state.thisParticipant.name,
            "currentScore": state.thisParticipant.currentScore,
            "isSessionModerator": state.thisParticipant.isSessionModerator
        };
        window.sessionStorage.setItem("participant", JSON.stringify(participant));
        state.thisParticipant = participant;
    }
}

let setSessionModerator = () => {
    let sessionModeratorExists = false;
    for(let i in state.participants) {
        if (state.participants[i].isSessionModerator) {
            state.sessionModerator = state.participants[i];
            sessionModeratorExists = true;
            break;
        }
    }
    if (!sessionModeratorExists) {
        state.sessionModerator = null;
    }
}

let drawParticipantsTable = () => {
    $('#participants').html("");
    for (let i in state.participants) {
        let participant = state.participants[i];
        let scoreDisplay = '';
        if (participant.currentScore) {
            scoreDisplay = (state.revealScores || (parseInt(state.clientId) === parseInt(participant.clientId))) ? participant.currentScore : '<i class="fas fa-check-circle fa-lg"></i>';
        } else {
            scoreDisplay = '';
        }
        let rowHtml = '<tr' + (parseInt(participant.clientId) === parseInt(state.clientId) ? ' class="myRow"' : '') + '>';
        rowHtml += '<td class="remove-participant" data-clientid="' + participant.clientId + '"><i class="fas fa-user-times fa-lg"></i></td>';
        rowHtml += '<td class="participant-name">' + sanitize(participant.name) + (participant.isSessionModerator ? ' <strong>&ast;</strong>' : '') + '</td>';
        rowHtml += '<td class="current-score">' + scoreDisplay + '</td>';
        rowHtml += '</tr>';
        $('#participants').append(rowHtml);
    }
}

let pendingTicketState = () => {
    $('.score-btn').removeAttr("disabled").removeClass("disabled");
    $('#reveal-scores').show();
    $('#clear-scores').hide();
    $('#start-pointing-btn').attr('disabled', 'disabled');
    $('#ticket-description-text').text(state.activeTicket.description);
    $('#ticket-text-input').val(state.activeTicket.description);
    $('#ticket-average-score-container').hide();
    $('#ticket-average-score').text("");
}

let revealedTicketState = () => {
    $('.score-btn').removeAttr("disabled").removeClass("disabled");
    $('#reveal-scores').hide();
    $('#clear-scores').show();
    $('#start-pointing-btn').attr('disabled', 'disabled');
    $('#ticket-description-text').text(state.activeTicket.description);
    $('#ticket-text-input').val(state.activeTicket.description);
    $('#ticket-average-score-container').show();
    $('#ticket-average-score').text(state.activeTicket.averageScore);
}

let noActiveTicketState = () => {
    $('.score-btn').attr("disabled", "disabled").addClass("disabled").removeClass("active");
    $('#moderator-buttons').css("visibility", "hidden");
    $('#start-pointing-btn').removeAttr('disabled');
    $('#ticket-description-text').text("");
    $('#ticket-average-score-container').hide();
    $('#ticket-average-score').text("");
}

let moderatorSettingsState = () => {
    if (state.thisParticipant && state.thisParticipant.isSessionModerator) {
        $('#ticket-input').show();
        $('#session-moderator-chx-container').show();
        $('#session-moderator-chx').attr('checked', 'checked');
        $('#moderator-buttons').css("visibility", "visible");
        $('#clear-session').show();
        $('#fist-of-five-toggle').show();
    } else {
        $('#ticket-input').hide();
        $('#clear-session').hide();
        $('#moderator-buttons').css("visibility", "hidden");
        $('#session-moderator-chx-container').hide();
        $('#session-moderator-chx').removeAttr('checked');
        $('#fist-of-five-toggle').hide();
    }
    
    // if there is no session moderator, show the checkbox toggle for everybody.
    if (!state.sessionModerator) {
        $('#session-moderator-chx-container').show();
        $('#session-moderator-chx').removeAttr('checked');
    }
}

let selectSessionState = () => {
    $('#existing-session-container').hide();
    $('#session-select-container').show();
    $('#clear-session').hide();
}

let activeSessionState = () => {
    $('#existing-session-container').show();
    $('#session-select-container').hide();
    $('#display-session-id').text(state.sessionLabel);
}

let fistOfFiveState = () => {
    if (state.pointingFistOfFive) {
        $('#fist-of-five-toggle').addClass('active-fof');
        $('.score-btn').addClass('fist-of-five-btn');
    } else {
        $('#fist-of-five-toggle').removeClass('active-fof');
        $('#ticket-text-input').val("");
        $('.score-btn').removeClass('fist-of-five-btn');
    }
}

let hasExistingParticipant = () => {
    for (let i in state.participants) {
        if (state.clientId === state.participants[i].clientId) {
            return true;
        }
    }

    return false;
}

let drawScoreOptions = () => {
    let scoreOptions = ["1", "2", "3", "5", "8", "13", "???"];
    let fistOfFiveOptions = ["1", "2", "3", "4", "5", "???"];
    let fistOfFiveIconMap = [
        "<i class=\"far fa-sad-cry\"></i>",
        "<i class=\"far fa-frown\"></i>",
        "<i class=\"far fa-meh\"></i>",
        "<i class=\"far fa-smile\"></i>",
        "<i class=\"far fa-grin-beam\"></i>",
        ""
    ];

    let scoresIterator = state.pointingFistOfFive ? fistOfFiveOptions : scoreOptions;

    $("#score-options").html("");
    for (let i in scoresIterator) {
        let isActive = state.thisParticipant && state.thisParticipant.currentScore ? (scoresIterator[i] === state.thisParticipant.currentScore ? 'active' : '') : '';
        let optionButton = '<div class="score-container"><button class="score-btn ' + isActive + '" value="' + scoresIterator[i] + '">' + (state.pointingFistOfFive ? scoresIterator[i] + " " + fistOfFiveIconMap[i] : scoresIterator[i]) + '</button></div>';
        $("#score-options").append(optionButton);
    }
    $("#score-options").append('<div style="clear: both;"></div>');
}

let drawSessionSelectTable = availableSessions => {
    $('#existing-sessions').empty();
    for (let i in availableSessions) {
        let rowHtml = '<tr class="available-session-row">';
        rowHtml += `<td data-sessionid="${availableSessions[i].sessionId}">`;
        rowHtml += `<a class="session-id-link"><i class="fas fa-door-open"></i> ${availableSessions[i].sessionLabel}</a>`;
        rowHtml += '</td>';
        rowHtml += '</tr>';
        $('#existing-sessions').append(rowHtml);
    }
}

let sendMessage = (action, data) => {
    let msg = Object.assign({}, data, {action, sessionId : state.sessionId});
    conn.send(JSON.stringify(msg));
}

let clearClientSession = () => {
    window.sessionStorage.removeItem("pointingPokerSessionId");
    window.sessionStorage.removeItem("participant");
    state.sessionId = null;
    state.thisParticipant = null;
    sendMessage("getAvailableSessions", {});
}

let sanitize = string => {
    const map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#x27;',
        "/": '&#x2F;',
    };
    
    if (string) {
        const reg = /[&<>"'/]/ig;
        return string.replace(reg, match => (map[match]));
    }
}