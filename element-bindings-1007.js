/** Place UI element event bindings in this file **/
/** ******************************************** **/

/** Submit Start Pointing New Ticket Form **/
$('body').on('submit', '#ticket-input', e => {
    e.preventDefault();
    if (!$('#ticket-text-input').val()) {
        alert("Please enter the JIRA ticket number or some description for this ticket.");
        return;
    }
    let msg = {
        "description": $('#ticket-text-input').val()
    };
    sendMessage("addTicket", msg);
    return false;
});
/** ******************************* **/


/** Click Remove Participant Icon **/
$('body').on('click', '.remove-participant', e => {
    let msg = {
        "removalClientId": $(e.currentTarget).data("clientid")
    };
    sendMessage("removeParticipant", msg);
});
/** ******************************* **/


/** Click on a score button **/
$('body').on('click', '.score-btn', e => {
    let msg = {
        "score": $(e.currentTarget).val()
    };
    sendMessage("updateParticipant", msg);
});
/** ******************************* **/


/** Click on Reveal Scores Button **/
$('body').on('click', '#reveal-scores', () => {
    sendMessage("revealScores", {});
});
/** ******************************* **/


/** Click on the Clear Scores Button **/
$('body').on('click', '#clear-scores-btn', () => {
    sendMessage("closeTicket", {});
    $('#ticket-text-input').val("");
});
/** ******************************* **/


/** Change state of Session Moderator checkbox **/
$('body').on('change', '#session-moderator-chx', e => {
    let isSessionModerator = $(e.currentTarget).is(':checked') ? true : false;

    let msg = {
        isSessionModerator
    };
    sendMessage("updateParticipant", msg);
});
/** ******************************* **/



/** Submit the form for adding or updating participant name **/
$('body').on('submit', '#add-to-session', e => {
    if (!$('#participant-name').val()) {
        alert("Please enter your name!");
        return false;
    }
    e.preventDefault();
    let action =  hasExistingParticipant() ? "updateParticipant" : "addParticipant";
    let msg = {
        "name": $('#participant-name').val()
    };
    sendMessage(action, msg);
    return false;
});
/** ******************************* **/

$('body').on('click', '#existing-sessions td', e => {
    let selectedSessionId = $(e.currentTarget).data('sessionid');
    window.sessionStorage.setItem("pointingPokerSessionId", selectedSessionId);
    state.sessionId = selectedSessionId;
    sendMessage("enterSession");
    activeSessionState();
});



$('body').on('click', '#back-to-session-select', () => {
    sendMessage("exitSession");
    selectSessionState();
    clearClientSession();
});

$('body').on('click', '#clear-session', () => {
    let rconfirm = confirm("Are you sure you want to end this session? All participants will be removed.");
    if (!rconfirm) {
        return;
    }
    sendMessage("clearSession", {});
});

$('body').on('click', '#history-toggle-link', () => {
    if ($('#history-container').is(':visible')) {
        $('#history-container').hide();
        $('i.fa-caret-square-up').hide();
        $('i.fa-caret-square-down').show();
    } else {
        $('#history-container').show();
        $('i.fa-caret-square-up').show();
        $('i.fa-caret-square-down').hide();
    }
});

$('body').on('click', '#fist-of-five-toggle', () => {
    sendMessage("toggleFistOfFive", {});
});